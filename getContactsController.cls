/**
 * @description       : 
 * @author            : kavya.konagonda@mtxb2b.com
 * @group             : 
 * @last modified on  : 10-28-2020
 * @last modified by  : kavya.konagonda@mtxb2b.com
 * Modifications Log 
 * Ver   Date         Author                       Modification
 * 1.0   10-26-2020   kavya.konagonda@mtxb2b.com   Initial Version
**/
public with sharing class getContactsController {

    @AuraEnabled(cacheable=true)
    public static List<Contact> search() {
        List<Contact> contactList=new List<Contact>();
        if(
            Contact.SObjectType.getDescribe().isAccessible() &&
            Schema.SObjectType.Contact.fields.Name.isAccessible() &&
            Schema.SObjectType.Contact.fields.Phone.isAccessible()
        )
        {
            contactList=[SELECT Id,Name,Birthdate,Email FROM Contact LIMIT 5]; 
        }
    return contactList;        
    }
}
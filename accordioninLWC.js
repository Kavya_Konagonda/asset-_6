import { LightningElement ,wire,track} from 'lwc';
import retriveContacts  from '@salesforce/apex/getContactsController.search';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
export default class AccordinLWC extends LightningElement {
    @track contacts;
    @wire(retriveContacts)
    wiredContacts({ error, data }) {
        if(data) {
            this.contacts = data;
            this.error = undefined;
        }
        else if(error) {
            this.error = error;
            this.contacts = undefined;
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error',
                    message: error.message,
                    variant: 'error',
                }),
            );
        }
    }
}